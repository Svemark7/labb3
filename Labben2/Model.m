//
//  Model.m
//  Labben2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "Model.h"

@interface Model()

@property ( nonatomic) NSDictionary *question1;
@property ( nonatomic) NSDictionary *question2;
@property ( nonatomic) NSDictionary *question3;
@property ( nonatomic) NSDictionary *question4;
@property ( nonatomic) NSDictionary *question5;
@property ( nonatomic) NSDictionary *question6;
@property ( nonatomic) NSDictionary *question7;
@property ( nonatomic) NSDictionary *question8;
@property ( nonatomic) NSDictionary *question9;
@property ( nonatomic) NSDictionary *question10;
@property ( nonatomic) NSArray *questions;

@end

@implementation Model

-(instancetype)init {
    self = [super init];
    if(self) {
        self.question1 = @{@"question": @"How many mushrooms exists in the forrest?",
                           @"correct": @"Seven",
                           @"answer2": @"Three",
                           @"answer3": @"One",
                           @"answer4": @"Nine",
                           @"hint": @"More than six"};
        
        self.question2 = @{@"question": @"How many mushrooms exists in the city?",
                           @"correct": @"Two",
                           @"answer2": @"Three",
                           @"answer3": @"One",
                           @"answer4": @"Nine",
                           @"hint": @"Less than Three"};
        
        self.question3 = @{@"question": @"Three plus two equals?",
                           @"correct": @"Five",
                           @"answer2": @"Three",
                           @"answer3": @"Six",
                           @"answer4": @"Seven",
                           @"hint": @"Not Seven"};
        
        self.question4 = @{@"question": @"There's four cars at the parking lot!",
                           @"correct": @"Four",
                           @"answer2": @"Three",
                           @"answer3": @"Five",
                           @"answer4": @"Nine",
                           @"hint": @"FOUR"};
        
        self.question5 = @{@"question": @"What does the fish say?",
                           @"correct": @"Blub",
                           @"answer2": @"Quack",
                           @"answer3": @"Moooo",
                           @"answer4": @"Mjau",
                           @"hint": @"Quack. Jk, it's not"};
        
        self.question6 = @{@"question": @"A brown cookie often tastes like ..?",
                           @"correct": @"Chocolate",
                           @"answer2": @"Vanilla",
                           @"answer3": @"Blueberry",
                           @"answer4": @"Lime",
                           @"hint": @"Yuuumie"};
        
        self.question7 = @{@"question": @"A bicycle usually has?",
                           @"correct": @"2 wheels",
                           @"answer2": @"1 wheel",
                           @"answer3": @"3 wheels",
                           @"answer4": @"8 wheels",
                           @"hint": @"Less than 3"};
        
        self.question8 = @{@"question": @"Food is something you ..?",
                           @"correct": @"Eat",
                           @"answer2": @"Ride",
                           @"answer3": @"Drive",
                           @"answer4": @"Hate",
                           @"hint": @"Consume"};
        
        self.question9 = @{@"question": @"How old is Fredrik Svemark?",
                           @"correct": @"22",
                           @"answer2": @"19",
                           @"answer3": @"25",
                           @"answer4": @"29",
                           @"hint": @"20 + 5 - 4 + 1"};
        
        self.question10 = @{@"question": @"Who made this Quiz?",
                            @"correct": @"Fredrik",
                            @"answer2": @"Erik",
                            @"answer3": @"Victor",
                            @"answer4": @"Daniel",
                            @"hint": @"He's not 25 years old"};
        self.questions = @[self.question1, self.question2, self.question3, self.question4, self.question5, self.question6, self.question7, self.question8, self.question9, self.question10];
    }
    return self;
}

-(NSDictionary*)modelGenerator {
    return self.questions [arc4random() % self.questions.count];
}

@end
