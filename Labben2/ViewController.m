//
//  ViewController.m
//  Labben2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "ViewController.h"
#import "Model.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *question;
@property (weak, nonatomic) IBOutlet UIButton *answer1;
@property (weak, nonatomic) IBOutlet UIButton *answer2;
@property (weak, nonatomic) IBOutlet UIButton *answer3;
@property (weak, nonatomic) IBOutlet UIButton *answer4;
@property (weak, nonatomic) IBOutlet UIButton *generateQuestion;
@property (nonatomic) IBOutlet NSDictionary *quest;
@property (nonatomic) Model *modelIns;
@property (weak, nonatomic) IBOutlet UIButton *HintButton;
@end

@implementation ViewController

@synthesize answer1, answer2, answer3, answer4, question, generateQuestion;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modelIns = [[Model alloc]init];
    self.quest = [[NSDictionary alloc]init];
}

- (IBAction)button1:(UIButton *)sender {
    if([sender.currentTitle isEqualToString:self.quest[@"correct"]]) {
        [self.question setText:@"Correct Answer!"];
    } else {
        [self.question setText:@"Incorrect Answer!"];
    }
}
- (IBAction)button2:(UIButton *)sender {
    if([sender.currentTitle isEqualToString:self.quest[@"correct"]]) {
        [self.question setText:@"Correct Answer!"];
    } else {
        [self.question setText:@"Incorrect Answer!"];
    }
}
- (IBAction)button3:(UIButton *)sender {
    if([sender.currentTitle isEqualToString:self.quest[@"correct"]]) {
        [self.question setText:@"Correct Answer!"];
    } else {
        [self.question setText:@"Incorrect Answer!"];
    }
}
- (IBAction)button4:(UIButton *)sender {
    if([sender.currentTitle isEqualToString:self.quest[@"correct"]]) {
        [self.question setText:@"Correct Answer!"];
    } else {
        [self.question setText:@"Incorrect Answer!"];
    }
}
- (IBAction)newQuestion:(id)sender {
    [self setUpQuestion];
}
- (IBAction)hint:(id)sender {
    
    NSString *title = @"Hint";
    NSString *hint = [self.quest objectForKey:@"hint"];
    NSString *thanks = @"Thanks";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:hint
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:thanks style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void) setUpQuestion {
    self.quest = [self.modelIns modelGenerator];
    [self.HintButton setTitle:@"Hint" forState:UIControlStateNormal];
    [self.question setText:[self.quest objectForKey:@"question"]];
    [self.answer1 setTitle:[self.quest objectForKey:@"correct"] forState:UIControlStateNormal];
    [self.answer2 setTitle:[self.quest objectForKey:@"answer2"] forState:UIControlStateNormal];
    [self.answer3 setTitle:[self.quest objectForKey:@"answer3"] forState:UIControlStateNormal];
    [self.answer4 setTitle:[self.quest objectForKey:@"answer4"] forState:UIControlStateNormal];
}

@end










