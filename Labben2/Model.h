//
//  Model.h
//  Labben2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

-(NSDictionary*)modelGenerator;

@end
